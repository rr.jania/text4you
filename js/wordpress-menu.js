var navToggleBtns = document.getElementsByClassName("nav-toggle-btn");
for(var i=0; i<navToggleBtns.length; i++) {
    navToggleBtns[i].addEventListener("click", function() {
        var dataTarget = this.getAttribute("data-target");
        if(dataTarget == null || dataTarget == "") {
            console.warn("#Wordpress-menu: Atrybut \"data-target\" dla przycisku rozwijającego menu jest nieprawidłowo zdefiniowany. Menu w wersji mobilnej może nie działać prawidłowo!");
            return false;
        }
        if(document.getElementById(dataTarget).style.display == "" || document.getElementById(dataTarget).style.display == "none") {
            document.getElementById(dataTarget).style.display = "block";
            var targetClass = document.getElementById(dataTarget).getAttribute("class");
            document.getElementById(dataTarget).setAttribute("class", targetClass + " slideInDown");
            this.innerHTML = "x";
        }
        else {
            document.getElementById(dataTarget).style.display = "";
            this.innerHTML = "≡";
        }
    })
}

var navDropdown = document.getElementsByClassName("menu-item-has-children");
for(var i=0; i<navDropdown.length; i++) {
    navDropdown[i].addEventListener("mouseenter", function() {
        var childrens = this.children;
        for(var j=0; j<childrens.length; j++) {
            if(childrens[j].getAttribute("class") == "sub-menu") {
                childrens[j].setAttribute("class", "sub-menu slideInDown");
            }
        }
    })
}

window.onscroll = function() {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) 
        document.getElementById("main-menu").className = "menu-fixed";
    else
        document.getElementById("main-menu").className = "container-fluid";
}