<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title>
        <?php wp_title(); ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/wordpress-menu.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/main.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/articles.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/pagination.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/sidebar.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/single.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/footer.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/fontello.css" />

    <!-- Fonts -->
    <!--<link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Anton&amp;subset=latin-ext" rel="stylesheet">

    <?php wp_head(); ?>
</head>

<body>
    <header id="top-header" class="container-fluid">

        <div id="baner" class="row">
            <div class="container">
                <div class="row">
                    <div id="baner-img" class="col-xs-12">
                        <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/baner.png" alt="TEXTOMA">
                    </div>
                </div>
            </div>
        </div>
        
        <div id="main-menu" class="container-fluid">

            <div class="row">
                <div id="top-bar"></div>
            </div>

            <div class="col-xs-12">

                <?php 
                $defaults = array(
                    'menu'            => 'main-nav',
                    'container'       => 'nav',
                    'container_class' => 'main-menu',
                    'menu_class'      => 'nav-menu',
                    'menu_id'         => 'main-menu-ul',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    //'items_wrap'      => '<ul class="%2$s">%3$s</ul>', //'<ul id="%1$s" class="%2$s">%3$s</ul>' - with ID
                    'depth'           => 2,
                    'walker'          => ''
                );

                wp_nav_menu( $defaults );
            ?>

                    <div class="nav-toggle-btn" data-target="main-menu-ul">≡</div>

            </div>
        </div>

    </header>