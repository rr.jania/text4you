<?php
    // Rejestracja menu
    register_nav_menus(array(

        'main-nav' => 'Górne menu strony',

    ));

    // Włączenie wspierania miniaturek postów
    add_theme_support( 'post-thumbnails');

function my_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='pagination'>"; 
         if($paged > 1) echo "<a href='".get_pagenum_link($paged - 1)."'><div><i class='demo-icon icon-left-open'></i></div></a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<a class='active'><div>".$i."</div></a>":"<a href='".get_pagenum_link($i)."' class='inactive' ><div>".$i."</div></a>";
             }
         }

         if ($paged < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'><div><i class='demo-icon icon-right-open'></i></div></a>";
         echo "</div>";
     }
}

/**
 * Tworzenie sidebara
 */
register_sidebar ( array(
  'name'          => 'main-sidebar',
  'description'   => 'Pasek boczny',
  'before_widget' => '<div class="panel">',
  'after_widget'  => '</div></div>',
  'before_title'  => '<div class="panel-title">',
  'after_title'   => '</div><div class="panel-body">'
));

?>