<!-- Templatka wyglądu strony błędu 404-->

<?php get_header(); ?>

    <!--Kod strony 404-->
    <div class="container content">
        <div class="row">
            <div class="col-md-12">
               
                <p class="text-center">STRONA NIE ZNALEZIONA :(</p>
                <a href="<?php bloginfo('url'); ?>" class="btn btn-primary btn-lg" role="button">Wróć na stronę główną</a>
                
            </div>
        </div>
    </div>

<?php get_footer(); ?>