<?php
    /* Template name: Strona główna */
?>

    <?php get_header(); ?>

        <!--Treść listy wpisów z bloga-->
        <div class="container content">
            <div class="row">
                <div class="col-md-8">

                    <!--Generowanie listy wpisów z bloga-->
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                        <article class="article-list">
                            <div class="title">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_title(); ?>
                                </a>
                            </div>
                            <div class="thumbnail">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_post_thumbnail( $size = 'medium' ); ?>
                                </a>
                            </div>
                            <div class="category">Kategorie:
                                <?php the_category() ?>
                            </div>
                            <div class="curious">
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="read-more">
                                <a href="<?php the_permalink(); ?>" class="btn btn-default">Czytaj więcej</a>
                            </div>
                        </article>

                        <?php endwhile; ?>
                            <!-- post navigation -->
                            <?php else: ?>
                                <!-- no posts found -->
                                <?php endif; ?>

                                    <?php my_pagination($wp_query->max_num_pages) ?>

                </div>
                <div class="col-md-4">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>


        <?php get_footer(); ?>