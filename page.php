<?php 
    /* Template name: Pojedyncza strona */
?>

    <!-- Templatka wyglądu pojedynczej strony-->

<?php get_header(); ?>
       
    <div class="container content">
        <div class="row">
            <div class="col-md-12">
               
                <!--Pobranie treści strony-->
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <!--Wyświetlenie tytułu strony-->
                    <?php the_title( $before = '<h3>', $after = '</h3>', $echo = true ) ?>
                        <!--Wyświetlenie treści strony-->
                    <?php the_content(); ?>
                <?php endwhile; ?>
                    <!-- post navigation -->
                <?php else: ?>
                    <!-- no posts found -->
                <?php endif; ?>
                
            </div>
        </div>
    </div>
    
<?php get_footer(); ?>