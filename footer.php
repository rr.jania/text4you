<footer class="container-fluid">
    <div class="container">
        <div class="row">
            <div id="footer" class="col-xs-12">
                <span>Znajdź mnie: </span>
                <a href="#" class="icon"><i class="demo-icon icon-linkedin"></i></a>
                <a href="#" class="icon"><i class="demo-icon icon-facebook"></i></a>
            </div>
        </div>
    </div>
</footer>
<script src="<?php bloginfo('template_url'); ?>/js/wordpress-menu.js"></script>

<?php wp_footer();?>
    </body>

    </html>