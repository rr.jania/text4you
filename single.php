<!-- Templatka wyglądu pojedyczego wpisu na blogu-->

<?php get_header(); ?>

    <div class="container content">
        <div class="row">
            <div class="col-md-8">

                <!--Pobranie treści pojedynczego wpisu-->
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                    <div>
                        <div class="single-title">
                            <?php the_title(); ?>
                        </div>
                        <div class="category">Kategorie:
                            <?php the_category() ?>
                        </div>
                    </div>


                    <div>
                        <!--Wyświetlenie miniaturki wpisu-->
                        <img src="<?php the_post_thumbnail_url() ?>" alt="" style="width:100%;" class="img-responsive">

                            <!--Wyświetlenie treści pojedynczego wpisu-->
                            <?php the_content(); ?>
                    </div>

                    <div class="single-pagination">
                        <div class="previous-post">
                            <?php previous_post_link('%link', '<i class="demo-icon icon-left-open"></i>%title'); ?>
                        </div>
                        <div class="next-post">
                            <?php next_post_link('%link', '%title<i class="icon-right-open"></i>') ?>
                        </div>
                    </div>


                    <?php endwhile; ?>
                        <!-- post navigation -->
                        <?php else: ?>
                            <!-- no posts found -->
                            <?php endif; ?>

            </div>
            <div class="col-md-4">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>

    <?php get_footer(); ?>